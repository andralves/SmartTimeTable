#ifndef OVERLAPMODE_H
#define OVERLAPMODE_H

enum OverlapMode{
    /**
     * No overlap mode
     */
    NONE,
    /**
     * Partial overlap mode
     */
    PARTIAL
};

#endif // OVERLAPMODE_H
