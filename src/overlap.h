#ifndef OVERLAP_H
#define OVERLAP_H

#include <QtCore/qglobal.h>
#include "aula.h"

class Overlap {

public:
    Overlap();
    Overlap(Aula a, Hora overTime, int count);
    void incrementCount();
    void incrementTime(Hora h);
    Aula getAula() const;
    Hora getOverTime() const;
    int getCount() const;
    bool operator==(Overlap& obj);
private:
    Aula aula;
    Hora overTime;
    int count;

};

#endif // OVERLAP_H
