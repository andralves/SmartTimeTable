#include "overlap.h"

Overlap::Overlap()
{

}
Overlap::Overlap(Aula a, Hora overTime, int count) {
    this->aula = a;
    this->overTime = overTime;
    this->count = count;
}

void Overlap::incrementCount() {
    this->count++;
}

void Overlap::incrementTime(Hora h) {
    int totalMins = this->overTime.getTotalTimeInMins() + h.getTotalTimeInMins();
    Hora* newHour = new Hora(totalMins);
    this->overTime = *newHour;
    free(newHour);
}

Aula Overlap::getAula() const {
    return aula;
}

Hora Overlap::getOverTime() const {
    return overTime;
}

int Overlap::getCount() const {
    return count;
}

bool Overlap::operator==(Overlap& obj) {
    if (!(this->aula == obj.aula)) {
        return false;
    }
    if (!(this->overTime == obj.overTime)) {
        return false;
    }
    if (!(this->count == obj.count)) {
        return false;
    }
    return true;
}
