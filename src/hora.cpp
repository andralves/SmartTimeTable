#include "hora.h"


Hora::Hora(){}
    /**
     * Constructs an object given the hours and minutes.
     *
     * @param h The hour.
     * @param m The minutes.
     * @throws IllegalArgumentException if h < 0 || m < 0 || m > 59
     */
Hora::Hora(int h, int m) {
    if (!isValid(h, m)) {
        throw std::invalid_argument( "Erro: Um ou mais parametros de entrada nao validos!" );
    }

    this->min = (h * 60) + m;
}

    /**
     * Constructs an object given the total number of minutes.
     *
     * @param m The minutes.
     * @throws IllegalArgumentException if m < 0
     */
Hora::Hora(int m) {
    if (m < 0) {
        throw std::invalid_argument( "Erro: Um ou mais parametros de entrada nao validos!" );
    }

    this->min = m;
}

    /**
     * Returns the hour this object represents.
     *
     * @return The hour.
     */
int Hora::getHora() const {
    return min / 60;
}

    /**
     * Returns the minutes this object represents.
     *
     * @return The minutes.
     */
int Hora::getMinutes() const {
    return min % 60;
}

/**
 * Returns the total amount of time this object represents in minutes.
 *
 * @return The minutes.
 */
int Hora::getTotalTimeInMins() const {
    return min;
}

/**
 * This function determines the amount of time between two Hora objects
 *
 * @param h1 First Hora
 * @param h2 Second Hora
 * @return Returns a Hora object representing the time between the given
 * Hora objects.
 */
Hora Hora::timeLapse(Hora *h1, Hora *h2) {
    int mins = std::abs(h1->getMinutes() - h2->getMinutes());
    Hora h = Hora(mins);
    return h;
}

/**
 * @param h The hours
 * @param m The minutes
 * @return true if values are valid, false otherwise
 */
bool Hora::isValid(int h, int m) {
    if (h < 0) {
        return false;
    }

    if (m < 0 || m > 59) {
        return false;
    }

    return true;
}

QString Hora::toString() {
    QString s = "";

    s += this->min / 60;

    if (s.length() == 1) {
        s = "0" + s;
    }

    s += ":" + this->min % 60;

    if (s.length() == 4) {
        s += "0";
    }

    return s;
}

bool Hora::operator==(const Hora& obj){
    return this->min == obj.min;
}

bool Hora::operator!=(const Hora& obj){
    return this->min != obj.min;
}

int Hora::compareTo(Hora *arg0) {

    if (this->min < arg0->min) {
        return -1;
    }

    if (this->min > arg0->min) {
        return 1;
    }

    return 0;
}
