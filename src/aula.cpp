#include "aula.h"

Aula::Aula()
{

}

/**
 * Constructs a Aula object.
 *
 * @param ds The day of the week this class is lectured.
 * @param inicio The starting hour.
 * @param fim The finishing hour.
 * @throws IllegalArgumentException if any parameter is null or if the class
 * number is under 1.
 */
Aula::Aula(DiaSemanal ds, Hora *inicio, Hora *fim) {
    this->dia = ds;
    this->inicio = inicio;
    this->fim = fim;
}

/**
 * @return the dia
 */
DiaSemanal Aula::getDia() {
    return dia;
}

/**
 * @return the inicio
 */
Hora *Aula::getInicio() {
    return inicio;
}

/**
 * @return the fim
 */
Hora *Aula::getFim() {
    return fim;
}

/**
 * Calculates the duration of the class. It calls the timeLapse() function
 * with the start and end of the class as parameters.
 *
 * @return The duration in a Hora object.
 */
Hora Aula::getDuration() {
    Hora h;
    h = Hora::timeLapse(this->inicio, this->fim);
    return h;
}

QString Aula::toString() {
    return getDia() + " " + getInicio()->toString() + " " + getFim()->toString();
}

bool Aula::operator==(const Aula& obj) const{
    return this->dia == obj.dia && this->inicio == obj.inicio && this->fim == obj.fim;
}

int Aula::compareTo(Aula *arg0) {
    if (*this == *arg0) {
        return 0;
    }

    if (getDia() < arg0->getDia()) {
        return -1;
    }

    if (getDia() > arg0->getDia()){
        return 1;
    }

    int ini = this->inicio->compareTo(arg0->inicio);

    if (ini != 0) {
        return ini;
    }

    return 1;
}

void Aula::changeTo(Aula aula) {
    this->dia = aula.getDia();
    this->inicio = aula.getInicio();
    this->fim = aula.getFim();
}

QString Aula::toXML() {
    QString out = "";

    out += "\t\t\t<day>" + ToString(dia) + "</day>\n";
    out += "\t\t\t<start>" + QString::number(inicio->getTotalTimeInMins()) + "</start>\n";
    out += "\t\t\t<finish>" + QString::number(fim->getTotalTimeInMins()) + "</finish>\n";

    return out;
}
