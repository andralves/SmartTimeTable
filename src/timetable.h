#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <QtCore/qglobal.h>
#include <QList>
#include <QStringList>
#include <QString>

class Timetable
{
public:
    Timetable();

private:
    int fileID;
    //QList<Turma>
    QStringList cadeiras;
    bool deadlock;
};

#endif // TIMETABLE_H
