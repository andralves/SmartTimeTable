#include "OverlapOptions.h"

OverlapOptions::OverlapOptions()
{

}
OverlapOptions::OverlapOptions(OverlapMode overlapMode, int *count, Hora *time) {
    this->overlapmode = overlapMode;
    this->overlapcount_lim = count;
    this->overlaptime_lim = time;
    this->overlappingCount = 0;
    this->overlappingTime = Hora(0);
}

OverlapMode OverlapOptions::getOverlapmode() {
    return overlapmode;
}

Hora *OverlapOptions::getOverlaptime() {
    return overlaptime_lim;
}

int *OverlapOptions::getOverlapcount() {
    return overlapcount_lim;
}

bool OverlapOptions::crossLimits(Aula *a, Aula *b) {
    if(overlapmode == OverlapMode::NONE) {
        return true;
    }

    int *limCount;
    Hora *limTime, temp, overtime;

    if(!overlaps->contains(*a)) {

        overlaps->insert(*a, Overlap(*a, Hora(0), 0));
    }

    if ((limCount = getOverlapcount()) != NULL) {
        if (overlappingCount < *limCount) {
            overlappingCount++;
            Overlap tmp = overlaps->value(*a);
            tmp.incrementCount();
            overlaps->insert(*a, tmp);
            free(&tmp);
        } else {
            overlappingCount -= overlaps->value(*a).getCount();
            overlaps->remove(*a);
            return true;
        }
    }

    limTime = getOverlaptime();
    if(limTime != NULL) {
        if(a->getInicio()->compareTo(b->getInicio()) < 0) {
            overtime = Hora::timeLapse(a->getFim(), b->getInicio());
            temp = Hora(overlappingTime.getTotalTimeInMins() + overtime.getTotalTimeInMins());
        } else {
            overtime = Hora::timeLapse(a->getInicio(), b->getFim());
            temp = Hora(overlappingTime.getTotalTimeInMins() + overtime.getTotalTimeInMins());
        }

        if(temp.compareTo(limTime) <= 0) {
            overlappingTime = temp;
            Overlap tmp = overlaps->value(*a);
            tmp.incrementTime(overtime);
            overlaps->insert(*a, tmp);
            free(&tmp);
        } else {
            // Se isto falhou é preciso decrementar o count se este não for null nas opções
            if ((limCount = getOverlapcount()) != NULL) {
                overlappingCount -= overlaps->value(*a).getCount();
            }
            overlappingTime = Hora(overlappingTime.getTotalTimeInMins() - overlaps->value(*a).getOverTime().getTotalTimeInMins());
            overlaps->remove(*a);
            return true;
        }
    }

    return false;
}

void OverlapOptions::removeFromLim(Turma t) {
    for(Aula* a : t.getAulas()) {
        if(overlaps->contains(*a)) {
            overlappingCount -= overlaps->value(*a).getCount();
            overlappingTime = Hora(overlappingTime.getTotalTimeInMins() - overlaps->value(*a).getOverTime().getTotalTimeInMins());
            overlaps->remove(*a);
        }
    }
}

void OverlapOptions::resetLim() {
    this->overlappingCount = 0;
    this->overlappingTime = Hora(0);
    this->overlaps->clear();
}

QString OverlapOptions::toXML() {

    QString OM = "NONE";
    if(overlapmode == OverlapMode::PARTIAL) OM = "PARTIAL";

    QString out = "";

    out += "\t<overlapping>\n";
    out += "\t\t<mode>" + OM + "</mode>\n";
    out += "\t\t<count>" + QString::number(*overlapcount_lim) + "</count>\n";
    out += "\t\t<time>";
    if(overlaptime_lim == NULL) {
        out += "null";
    } else {
        out += overlaptime_lim->getTotalTimeInMins();
    }
    out += "</time>\n";
    out += "\t</overlapping>\n";

    return out;
}
