#include "turma.h"

Turma::Turma()
{

}

Turma::Turma(QString c, Componente cmp, QString t) {
    if (c == NULL || t == NULL) {
        printf("Erro: Um ou mais parametros de entradas estao incorrectos.");
    }

    this->cadeira = c;
    this->comp = cmp;
    this->turma = t;
    this->aulas = QSet<Aula*>();
    this->valid = false; /* ********************************************************************************* */
    this->include = true;
}

void Turma::setCadeira(QString cadeira) {
    this->cadeira = cadeira;
}

void Turma::setComp(Componente comp) {
    this->comp = comp;
}

void Turma::setTurma(QString turma) {
    this->turma = turma;
}

//bool Turma::addAula(Aula a) {
void Turma::addAula(Aula *a) {
    aulas.insert(a);
}

bool Turma::removeAula(Aula *a) {
    return aulas.remove(a);
}

/**
 * @return the valid
 */
bool Turma::isValid() {
    return valid;
}

/**
 * @param valid the valid to set
 */
void Turma::setValid(bool valid) {
    this->valid = valid;
}

void Turma::setInclude(bool include) {
    this->include = include;
}

bool Turma::toBeIncluded() {
    return this->include;
}

QString Turma::getCadeira() {
    return cadeira;
}

Componente Turma::getComponente() {
    return comp;
}

QString Turma::getTurma() {
    return turma;
}

QSet<Aula*> Turma::getAulas(){
    return aulas;
}

bool Turma::operator==(const Turma& obj) {
//    return this->dia == obj.dia && this->inicio == obj.inicio && this->fim == obj.fim;
    return this->cadeira == obj.cadeira && this->comp == obj.comp && this->turma == obj.turma;
}

QString Turma::toString() {
    return getCadeira() + " - " + getComponente() + getTurma();
}

QString Turma::toStringCompact() {
    if (getCadeira().length() < 3) {
        return getCadeira().toUpper() + "-" + getComponente() + getTurma();
    }

    return getCadeira().left(3).toUpper() + "-" + getComponente() + getTurma();
}

bool Turma::existsInTime(int i, Hora h) {
    for (Aula* a : aulas) {
        if (static_cast<int>(a->getDia()) == i) {
            if (h.compareTo(a->getInicio()) >= 0 && h.compareTo(a->getFim()) < 0) {
                return true;
            }
        }
    }

    return false;
}

int Turma::compareTo(Turma arg0) {
    return (getCadeira() + getComponente() + getTurma()).compare(arg0.getCadeira() + arg0.getComponente() + arg0.getTurma());
}

QString Turma::toXML() {
    QString component_string = "";
    switch (comp) {
        case T: component_string = "T"; break;
        case TP: component_string = "TP"; break;
        case P: component_string = "P"; break;
        case PL: component_string = "PL"; break;
        case OT: component_string = "OT"; break;
    }
    QString include_string = "False";
    if(include == true) include_string == "True";

    QString out = "";

    out += "\t\t<comp>" + component_string + "</comp>\n";
    out += "\t\t<descr>" + turma + "</descr>\n";
    out += "\t\t<include>" + include_string + "</include>\n";

    for(Aula* a : aulas) {
        out += "\t\t<lecture>\n";
        out += a->toXML();
        out += "\t\t</lecture>\n";
    }

    return out;
}
