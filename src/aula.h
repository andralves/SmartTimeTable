#ifndef AULA_H
#define AULA_H

#include <QtCore/qglobal.h>
#include "DiaSemanal.h"
#include "hora.h"

class Aula {

public:
    Aula();
    Aula(DiaSemanal ds, Hora *inicio, Hora *fim);
    DiaSemanal getDia();
    Hora *getInicio();
    Hora *getFim();
    Hora getDuration();
    QString toString();
    bool operator==(const Aula& obj) const;
    int compareTo(Aula *arg0);
    void changeTo(Aula aula);
    QString toXML();


private:
    DiaSemanal dia;
    Hora *inicio;
    Hora *fim;

};

#endif // AULA_H
