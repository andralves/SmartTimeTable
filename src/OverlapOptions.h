#ifndef OVERLAPOPTIONS_H
#define OVERLAPOPTIONS_H

#include <QtCore/qglobal.h>
#include <QHash>
#include "OverlapMode.h"
#include "overlap.h"
#include "aula.h"
#include "turma.h"

class OverlapOptions {

public:
    OverlapOptions();
    OverlapOptions(OverlapMode overlapMode, int *count, Hora *time);
    OverlapMode getOverlapmode();
    Hora* getOverlaptime();
    int *getOverlapcount();
    bool crossLimits(Aula *a, Aula *b);
    void removeFromLim(Turma t);
    void resetLim();
    QString toXML();

private:
    OverlapMode overlapmode;
    Hora *overlaptime_lim;
    int *overlapcount_lim;
    Hora overlappingTime;
    int overlappingCount;
    QHash<Aula, Overlap> *overlaps = new QHash<Aula, Overlap>();
};

#endif // OVERLAPOPTIONS_H
