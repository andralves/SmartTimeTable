#ifndef CONFLITABLE_H
#define CONFLITABLE_H

#include <QtCore/qglobal.h>
#include "aula.h"
#include "turma.h"
#include <OverlapOptions.h>

class Conflitable
{
public:
    Conflitable();
    bool conflictsWith(Aula *obj1, Aula *obj2, OverlapOptions oo);
    bool conflictsWith(Turma obj1, Turma obj2, OverlapOptions oo);
};

#endif // CONFLITABLE_H
