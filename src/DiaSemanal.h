#ifndef DIASEMANAL_H
#define DIASEMANAL_H

#include <QString>

enum DiaSemanal {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
};


inline const QString ToString(DiaSemanal d) {
    switch (d)
    {
        case Monday:   return "Monday";
        case Tuesday:   return "Tuesday";
        case Wednesday:   return "Wednesday";
        case Thursday:   return "Thursday";
        case Friday:   return "Friday";
        case Saturday:   return "Saturday";
    }
    return "Wrong day!";
}

#endif // DIASEMANAL_H
