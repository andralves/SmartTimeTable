#ifndef HORA_H
#define HORA_H

#include <stdexcept>
#include <QtCore/qglobal.h>
#include <QString>

class Hora
{
public:
    Hora();
    Hora(int h, int m);
    Hora(int m);
    int getHora() const;
    int getMinutes() const;
    int getTotalTimeInMins() const;
    static Hora timeLapse(Hora *h1, Hora *h2);
    bool isValid(int h, int m);
    QString toString();
    bool operator==(const Hora& obj);
    bool operator!=(const Hora& obj);
    int compareTo(Hora *arg0);

private:
    int min;

};

#endif // HORA_H
