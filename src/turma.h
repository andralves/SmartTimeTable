#ifndef TURMA_H
#define TURMA_H

#include <QtCore/qglobal.h>
#include "Componente.h"
#include <QSet>
#include "aula.h"

class Turma
{
public:
    Turma();
    Turma(QString c, Componente cmp, QString t);
    void setCadeira(QString cadeira);
    void setComp(Componente comp);
    void setTurma(QString turma);
    void addAula(Aula *a);
    bool removeAula(Aula *a);
    bool isValid();
    void setValid(bool valid);
    void setInclude(bool include);
    bool toBeIncluded();
    QString getCadeira();
    Componente getComponente();
    QString getTurma();
    QSet<Aula*> getAulas();
    bool operator==(const Turma& obj);
    QString toString();
    QString toStringCompact();
    bool existsInTime(int i, Hora h);
    int compareTo(Turma arg0);
    QString toXML();
private:
    QString cadeira;
    Componente comp; //Tem de pertencer a lista de limites na cadeira!
    QString turma;
    QSet<Aula*> aulas;
    bool valid;
    bool include;

};

#endif // TURMA_H
