#include "conflitable.h"

Conflitable::Conflitable()
{

}

bool Conflitable::conflictsWith(Aula *obj1, Aula *obj2, OverlapOptions oo) {

    /* Visto o caso anterior, duas aulas não entram em conflito se forem dadas em dias distintos */
    if (obj1->getDia() != obj2->getDia()) {
        return false;
    }

    /* Se forem dadas no mesmo dia elas entram em conflito caso comecem ou acabem ao mesmo tempo */
    if (obj1->getInicio() == obj2->getInicio() || obj1->getFim() == obj2->getFim()) {
        if (oo.crossLimits(obj1, obj2)) {
            return true;
        } else {
            return false;
        }
    }

    /* Se a aula "a" não terminar depois da aula "b" começar então não estão em conflito */
    if (obj1->getFim()->compareTo(obj2->getInicio()) <= 0) {
        return false;
    }

    /* Se a aula "b" não terminar depois da aula "a" começar então não estão em conflito */
    if (obj2->getFim()->compareTo(obj1->getInicio()) <= 0) {
        return false;
    }

    /* Caso nada se verifique para trás é porque estão sobrepostas e, logo, entram em conflito */
    if (oo.crossLimits(obj1, obj2)) {
        return true;
    }
    return false;
}

bool Conflitable::conflictsWith(Turma obj1, Turma obj2, OverlapOptions oo) {
    if (obj1.getCadeira() == obj2.getCadeira() && obj1.getComponente() == obj2.getComponente()) {
        return true;
    }

    for (Aula* a : obj1.getAulas()) {
        for (Aula* b : obj2.getAulas()) {
            if (conflictsWith(a, b, oo)) {
                return true;
            }
        }
    }

    return false;
}

